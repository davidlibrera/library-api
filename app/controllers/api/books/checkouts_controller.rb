module Api
  module Books
    class CheckoutsController < ::Api::BaseController
      before_action :authorize_user_from_access_token

      def index
        checkouts = book.checkouts.order(created_at: :desc)

        json = BookCheckoutSerializer.new(checkouts).serializable_hash

        render json: json, status: :ok
      end

      private

      def book
        @book ||= Book.find(params[:book_id])
      end
    end
  end
end
