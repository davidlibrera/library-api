module Api
  class BooksController < BaseController
    before_action :authorize_user_from_access_token, only: [:index, :show]
    before_action :authorize_admin_from_access_token, only: [:create, :update, :destroy]

    def index
      books = Book.all

      json = BookSerializer.new(books).serializable_hash

      render json: json, status: :ok
    end

    def show
      json = BookSerializer.new(book).serializable_hash

      render json: json, status: :ok
    end

    def create
      result = ::Books::Create.call(params: create_params)

      if result.success?
        json = BookSerializer.new(result.resource).serializable_hash
        render json: json, status: :ok
      else
        error = ApiError.new("INVALID", result.resource.errors)
        json = ApiErrorSerializer.new(error).serializable_hash

        render json: json, status: :unprocessable_entity
      end
    end

    def update
      result = ::Books::Update.call(resource: book, params: update_params)

      if result.success?
        json = BookSerializer.new(result.resource).serializable_hash
        render json: json, status: :ok
      else
        error = ApiError.new("INVALID", result.resource.errors)
        json = ApiErrorSerializer.new(error).serializable_hash

        render json: json, status: :unprocessable_entity
      end
    end

    def destroy
      book.destroy

      render json: {}, status: :no_content
    end

    private

    def book
      @book ||= Book.find(params[:id])
    end

    def create_params
      payload[:data][:attributes].slice(:title, :isbn, :available)
    end

    def update_params
      create_params
    end
  end
end
