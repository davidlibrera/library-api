module Api
  class UsersController < BaseController
    before_action :authorize_admin_from_access_token

    def index
      users = User.all

      json = UserSerializer.new(users).serializable_hash

      render json: json, status: :ok
    end

    def show
      json = UserSerializer.new(user).serializable_hash

      render json: json, status: :ok
    end

    def create
      result = Users::Create.call(params: create_params)

      if result.success?
        json = UserSerializer.new(result.resource).serializable_hash
        render json: json, status: :ok
      else
        error = ApiError.new("INVALID", result.resource.errors)
        json = ApiErrorSerializer.new(error).serializable_hash

        render json: json, status: :unprocessable_entity
      end
    end

    def update
      result = Users::Update.call(resource: user, params: update_params)

      if result.success?
        json = UserSerializer.new(result.resource).serializable_hash
        render json: json, status: :ok
      else
        error = ApiError.new("INVALID", result.resource.errors)
        json = ApiErrorSerializer.new(error).serializable_hash

        render json: json, status: :unprocessable_entity
      end
    end

    def destroy
      user.destroy

      render json: {}, status: :no_content
    end

    private

    def user
      @user ||= User.find(params[:id])
    end

    def create_params
      payload[:data][:attributes].slice(:email, :password, :password_confirmation, :role)
    end

    def update_params
      create_params
    end
  end
end
