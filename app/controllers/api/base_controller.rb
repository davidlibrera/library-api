module Api
  class BaseController < ApplicationController
    private

    def payload
      @payload ||= JSON.parse(request.body.read).deep_symbolize_keys
    end

    def authorize_admin_from_access_token
      render_unauthorized_error! if current_user.blank? || !current_user.admin?

      current_user
    end

    def authorize_user_from_access_token
      current_user || render_unauthorized_error!
    end

    def current_user
      session = Session.from_access_token(access_token)

      return nil if session.blank?

      session.user
    end

    def render_unauthorized_error!
      error = ApiError.new("UNAUTHORIZED")
      json = ApiErrorSerializer.new(error).serializable_hash

      render json: json, status: :unauthorized
    end

    def access_token
      return nil if request.env["HTTP_AUTHORIZATION"].blank?
      return nil if !request.env["HTTP_AUTHORIZATION"].match(/^Bearer (.?){1,}$/)

      @access_token ||=
        request.env["HTTP_AUTHORIZATION"].split("Bearer")[1].strip
    end
  end
end
