module Api
  class SessionsController < BaseController
    def create
      result = Sessions::Create.call(create_params)

      json = SessionSerializer.new(result.session).serializable_hash

      render json: json, status: :ok
    end

    private

    def create_params
      {
        email: payload[:data][:attributes][:email],
        password: payload[:data][:attributes][:password]
      }
    end
  end
end
