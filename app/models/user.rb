class User < ApplicationRecord
  validates :email, presence: true

  enum role: {admin: "admin", user: "user"}

  has_secure_password

  has_many :checkouts,
           class_name: "::BookCheckout",
           inverse_of: :user,
           dependent: :destroy
end
