class ApiError
  attr_reader :code
  attr_reader :details

  def initialize(code, details = {})
    @code = code
    @details = details
  end

  def attributes
    {code: code, details: details}
  end

  def id
    Digest::MD5.hexdigest(JSON.dump(attributes))[0..5]
  end
end
