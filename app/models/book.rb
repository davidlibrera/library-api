class Book < ApplicationRecord
  validates :title, presence: true
  validates :isbn, presence: true, uniqueness: {case_sensitive: false}

  has_many :checkouts,
           class_name: "::BookCheckout",
           inverse_of: :book,
           dependent: :destroy
end
