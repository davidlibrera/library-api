class Session
  attr_reader :user

  def initialize(user:)
    @user = user
  end

  def id
    access_token
  end

  def access_token
    @access_token ||=
      JWT.encode(
        jwt_payload,
        Rails.application.credentials.secret_key_base
      )
  end

  def self.from_access_token(access_token)
    payload, _opt = JWT.decode(
      access_token,
      Rails.application.credentials.secret_key_base
    )

    user = User.find(payload["user_id"])

    new(user: user) if user.present?
  rescue JWT::DecodeError, ActiveRecord::RecordNotFound
    nil
  end

  private

  def jwt_payload
    return {} if user.blank?

    {
      user_id: user.id
    }
  end
end
