class BookCheckout < ApplicationRecord
  belongs_to :book, inverse_of: :checkouts
  belongs_to :user, inverse_of: :checkouts
end
