class BookCheckoutSerializer
  include JSONAPI::Serializer

  attributes :created_at

  belongs_to :user
  belongs_to :book
end
