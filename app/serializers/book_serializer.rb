class BookSerializer
  include JSONAPI::Serializer
  attributes :title, :isbn, :available
end
