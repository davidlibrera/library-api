class ApiErrorSerializer
  include JSONAPI::Serializer
  attributes :code, :details
end
