module Books
  class Create
    include Interactor

    def call
      context.resource = resource

      resource.assign_attributes(params)
      context.fail! if !resource.valid?

      resource.save
    end

    private

    def resource
      @resource ||= Book.new
    end

    def params
      context.params
    end
  end
end
