module Books
  class Checkout
    include Interactor

    def call
      context.fail!(message: "book is not available") if !book.available

      Book.transaction do
        book.checkouts.create(user: user)

        book.update(available: false)
      end
    end

    private

    def book
      context.book
    end

    def user
      context.user
    end
  end
end
