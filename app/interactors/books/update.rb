module Books
  class Update
    include Interactor

    def call
      resource.assign_attributes(params)
      context.fail! if !resource.valid?

      resource.save
    end

    private

    def resource
      context.resource
    end

    def params
      context.params
    end
  end
end
