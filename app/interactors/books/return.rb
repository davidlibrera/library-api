module Books
  class Return
    include Interactor

    def call
      context.fail!(message: "Book is still available") if book.available

      context.fail!(message: "Book is not owned by user") if !owned_by_provided_user

      book.update(available: true)
    end

    private

    def owned_by_provided_user
      last_checkout.present? &&
        last_checkout.user_id == user.id
    end

    def last_checkout
      book.checkouts.order(id: :desc).first
    end

    def book
      context.book
    end

    def user
      context.user
    end
  end
end
