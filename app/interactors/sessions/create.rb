module Sessions
  class Create
    include Interactor

    def call
      if user.present?
        context.session = Session.new(user: user)
      else
        context.fail!(
          message: "Invalid credentials",
          code: "invalid_credentials"
        )
      end
    end

    private

    def user
      return @user if @user.present?

      @user = User.find_by(email: email)
      return @user if @user.blank?

      @user = @user.authenticate(password)
    end

    def email
      context.email
    end

    def password
      context.password
    end
  end
end
