FactoryBot.define do
  factory :book_checkout do
    book { build(:book) }
    user { build(:user) }
  end
end
