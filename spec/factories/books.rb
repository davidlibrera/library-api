FactoryBot.define do
  sequence(:book_index) { |n| n }

  factory :book do
    transient do
      index { generate(:book_index) }
    end

    title { "Book #{index}" }
    isbn { "isbn-#{index}" }
    available { true }
  end
end
