FactoryBot.define do
  sequence(:user_index) { |n| n }
  factory :user do
    transient do
      index { generate(:user_index) }
    end

    email { "user-#{index}@example.com" }
    role { "user" }
    password { "Asdf12!!" }

    factory :admin do
      email { "admin-#{index}@example.com" }
      role { "admin" }
    end
  end
end
