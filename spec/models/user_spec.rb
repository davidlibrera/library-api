require "rails_helper"

RSpec.describe User, type: :model do
  it { is_expected.to validate_presence_of(:email) }

  it do
    expect(subject).to(
      define_enum_for(:role).
        with_values(user: "user", admin: "admin").
        backed_by_column_of_type(:string)
    )
  end
end
