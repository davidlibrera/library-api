require "rails_helper"

RSpec.describe Book, type: :model do
  subject { build(:book) }

  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_presence_of(:isbn) }
  it { is_expected.to validate_uniqueness_of(:isbn).case_insensitive }
end
