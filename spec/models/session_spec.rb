require "rails_helper"

RSpec.describe Session do
  subject(:session) { described_class.new(user: user) }

  let(:user) { create(:user) }

  describe "#user" do
    it "returns provided user" do
      expect(session.user).to eq user
    end
  end

  describe ".from_access_token" do
    let(:access_token) { session.access_token }

    let(:result) do
      described_class.from_access_token(access_token)
    end

    it "returns a session for the user" do
      expect(result.user).to eq session.user
    end

    context "when user doesn't exist" do
      it "returns nil" do
        user.destroy

        expect(result).to be_blank
      end
    end

    context "when access_token is invalid" do
      let(:access_token) { "invalid-access-token" }

      it "returns nil" do
        expect(result).to be_blank
      end
    end

    context "when access_token is nil" do
      let(:access_token) { nil }

      it "returns nil" do
        expect(result).to be_blank
      end
    end
  end
end
