require "rails_helper"

RSpec.describe "GET /api/users" do
  subject { get "/api/users", headers: headers }

  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let!(:user) { create(:user) }

  let(:access_token) { access_token_for(logged_user) }
  let(:logged_user) { create(:admin) }

  let(:expected_response) do
    {
      data: [
        {
          id: user.id.to_s,
          type: "user",
          attributes: hash_including(role: "user", email: user.email)
        }, {
          id: logged_user.id.to_s,
          type: "user",
          attributes: hash_including(role: "admin", email: logged_user.email)
        }
      ]
    }
  end

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "returns a json response with all users" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
