require "rails_helper"

RSpec.describe "DELETE /api/users/:id" do
  subject { delete "/api/users/#{user_id}", headers: headers }

  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(logged_user) }

  let(:logged_user) { create(:admin) }

  let(:user) { create(:user) }
  let(:user_id) { user.id }

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    it "succeeds" do
      subject

      expect(response).to have_http_status(:no_content)
    end

    it "deletes" do
      subject

      expect { user.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
