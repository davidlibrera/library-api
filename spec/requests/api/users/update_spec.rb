require "rails_helper"

RSpec.describe "PUT /api/users/:id" do
  subject { put "/api/users/#{user_id}", headers: headers }

  let(:headers) do
    {
      "RAW_POST_DATA" => payload.to_json,
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(logged_user) }

  let(:logged_user) { create(:admin) }

  let(:user) { create(:admin) }

  let(:user_id) { user.id }

  let(:payload) do
    {
      data: {
        id: user.id.to_s,
        type: "user",
        attributes: {
          email: "new-email@example.com",
          password: "NewPass12!",
          password_confirmation: "NewPass12!",
          role: "user"
        }
      }
    }
  end

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    let(:expected_response) do
      {
        data: {
          type: "user",
          id: be_a(String),
          attributes: {
            email: "new-email@example.com",
            role: "user"
          }
        }
      }
    end

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "updates the user" do
      subject

      user.reload

      expect(user).to have_attributes(email: "new-email@example.com", role: "user")
    end

    it "returns a valid user json response" do
      subject

      expect(json_response).to match(expected_response)
    end

    context "and payload is invalid" do
      let(:payload) do
        {
          data: {
            type: "user",
            id: be_a(String),
            attributes: {email: ""}
          }
        }
      end

      let(:expected_response) do
        {
          data: {
            type: "api_error",
            id: be_a(String),
            attributes: {
              code: "INVALID",
              details: hash_including(:email)
            }
          }
        }
      end

      it "fails" do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns an INVALID error" do
        subject

        expect(json_response).to match(expected_response)
      end
    end
  end
end
