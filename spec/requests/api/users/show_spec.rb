require "rails_helper"

RSpec.describe "GET /api/users/:id" do
  subject { get "/api/users/#{user_id}", headers: headers }

  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(logged_user) }

  let(:logged_user) { create(:admin) }

  let(:user) { create(:user) }
  let(:user_id) { user.id }

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    let(:expected_response) do
      {
        data: {
          type: "user",
          id: user.id.to_s,
          attributes: hash_including(role: user.role, email: user.email)
        }
      }
    end

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "returns a valid user json response" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
