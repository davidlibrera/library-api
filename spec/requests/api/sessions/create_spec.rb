require "rails_helper"

RSpec.describe "POST /api/session", type: :request do
  subject { post "/api/session", headers: headers }

  let(:headers) do
    {
      "RAW_POST_DATA" => payload.to_json
    }
  end

  let(:payload) do
    {
      data: {
        type: "user",
        attributes: {
          email: "test@example.com",
          password: "Asdf12??"
        }
      }
    }
  end

  let!(:user) { create(:user, email: "test@example.com", password: "Asdf12??") }

  let(:expected_response) do
    {
      data: {
        type: "session",
        id: be_a(String)
      }
    }
  end

  it "succeeds" do
    subject

    expect(response).to have_http_status(:ok)
  end

  it "returns json with a valid access token" do
    subject

    expect(json_response).to match(expected_response)
  end

  it "returns a valid access token as id of response" do
    subject

    access_token = json_data[:id]

    session = Session.from_access_token(access_token)

    expect(session.user.id).to eq user.id
  end
end
