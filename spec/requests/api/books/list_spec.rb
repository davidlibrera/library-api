require "rails_helper"

RSpec.describe "GET /api/books" do
  subject { get "/api/books", headers: headers }

  let(:headers) { {} }

  let!(:book_1) do
    create(:book, title: "Title 1", isbn: "12345", available: true)
  end

  let!(:book_2) do
    create(:book, title: "Title 2", isbn: "54321", available: false)
  end

  let(:expected_response) do
    {
      data: [
        {
          id: book_1.id.to_s,
          type: "book",
          attributes: {
            title: "Title 1",
            isbn: "12345",
            available: true
          }
        }, {
          id: book_2.id.to_s,
          type: "book",
          attributes: {
            title: "Title 2",
            isbn: "54321",
            available: false
          }
        }
      ]
    }
  end

  it_behaves_like "an endpoint under authorization"

  context "with valid user jwt in headers" do
    let(:headers) { headers_with_bearer(user) }

    let(:access_token) { access_token_for(user) }
    let(:user) { create(:user) }

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "returns a json response with all books" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
