require "rails_helper"

RSpec.describe "POST /api/books" do
  subject { post "/api/books", headers: headers }

  let(:headers) do
    {
      "RAW_POST_DATA" => payload.to_json,
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(user) }

  let(:user) { create(:admin) }

  let(:payload) do
    {
      data: {
        type: "book",
        attributes: {
          title: "New title",
          isbn: "An isbn",
          available: "false"
        }
      }
    }
  end

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    let(:expected_response) do
      {
        data: {
          type: "book",
          id: be_a(String),
          attributes: {
            title: "New title",
            isbn: "An isbn",
            available: false
          }
        }
      }
    end

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "creates a book" do
      expect { subject }.to change(Book, :count).by(1)
    end

    it "returns a valid book json response" do
      subject

      expect(json_response).to match(expected_response)
    end

    context "and payload is invalid" do
      let(:payload) do
        {
          data: {
            type: "book",
            id: be_a(String),
            attributes: {}
          }
        }
      end

      let(:expected_response) do
        {
          data: {
            type: "api_error",
            id: be_a(String),
            attributes: {
              code: "INVALID",
              details: hash_including(:isbn, :title)
            }
          }
        }
      end

      it "fails" do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns an INVALID error" do
        subject

        expect(json_response).to match(expected_response)
      end
    end
  end
end
