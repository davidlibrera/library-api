require "rails_helper"

RSpec.describe "GET /api/books/:book_id/checkouts" do
  subject { get "/api/books/#{id}/checkouts", headers: headers }

  let(:headers) { {} }

  let(:book) do
    create(:book, title: "Title 1", isbn: "12345", available: true)
  end

  let(:id) { book.id }

  let!(:checkout) { create(:book_checkout, book: book, user: checkout_user) }
  let(:checkout_user) { create(:user) }

  let(:expected_response) do
    {
      data: [
        {
          id: checkout.id.to_s,
          type: "book_checkout",
          attributes: hash_including(:created_at),
          relationships: {
            book: {
              data: {
                type: "book",
                id: book.id.to_s
              }
            },
            user: {
              data: {
                type: "user",
                id: checkout_user.id.to_s
              }
            }
          }
        }
      ]
    }
  end

  context "with valid user jwt in headers" do
    let(:headers) do
      {
        "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
      }
    end

    let(:access_token) { access_token_for(user) }
    let(:user) { create(:user) }

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "returns a json response with all book checkouts" do
      subject

      expect(json_response).to match(expected_response)
    end
  end

  context "without valid user jwt in headers" do
    let(:expected_response) do
      {
        data: {
          id: be_a(String),
          type: "api_error",
          attributes: {
            code: "UNAUTHORIZED",
            details: {}
          }
        }
      }
    end

    it "returns unauthorized status" do
      subject

      expect(response).to have_http_status(:unauthorized)
    end

    it "returns an UNAUTHORIZED error" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
