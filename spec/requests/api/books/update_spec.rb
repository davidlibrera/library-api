require "rails_helper"

RSpec.describe "PUT /api/books/:id" do
  subject { put "/api/books/#{book_id}", headers: headers }

  let(:headers) do
    {
      "RAW_POST_DATA" => payload.to_json,
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(user) }

  let(:user) { create(:admin) }

  let!(:book) { create(:book, available: true) }
  let(:book_id) { book.id }

  let(:payload) do
    {
      data: {
        id: book.id.to_s,
        type: "book",
        attributes: {
          title: "New title",
          isbn: "A new isbn",
          available: false
        }
      }
    }
  end

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    let(:expected_response) do
      {
        data: {
          type: "book",
          id: be_a(String),
          attributes: {
            title: "New title",
            isbn: "A new isbn",
            available: false
          }
        }
      }
    end

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "updates a book" do
      subject

      book.reload

      expect(book).to have_attributes(title: "New title", isbn: "A new isbn")
    end

    it "returns a valid book json response" do
      subject

      expect(json_response).to match(expected_response)
    end

    context "and payload is invalid" do
      let(:payload) do
        {
          data: {
            type: "book",
            id: be_a(String),
            attributes: {title: ""}
          }
        }
      end

      let(:expected_response) do
        {
          data: {
            type: "api_error",
            id: be_a(String),
            attributes: {
              code: "INVALID",
              details: hash_including(:title)
            }
          }
        }
      end

      it "fails" do
        subject

        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "returns an INVALID error" do
        subject

        expect(json_response).to match(expected_response)
      end
    end
  end
end
