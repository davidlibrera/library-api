require "rails_helper"

RSpec.describe "GET /api/books/:id" do
  subject { get "/api/books/#{book_id}", headers: headers }

  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(user) }

  let(:user) { create(:user) }

  let(:book) { create(:book, title: "My title", isbn: "My isbn") }
  let(:book_id) { book.id }

  it_behaves_like "an endpoint under authorization"

  context "with valid user jwt in headers" do
    let(:expected_response) do
      {
        data: {
          type: "book",
          id: be_a(String),
          attributes: {
            title: "My title",
            isbn: "My isbn",
            available: true
          }
        }
      }
    end

    it "succeeds" do
      subject

      expect(response).to have_http_status(:ok)
    end

    it "returns a valid book json response" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
