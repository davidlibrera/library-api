require "rails_helper"

RSpec.describe "DELETE /api/books/:id" do
  subject { delete "/api/books/#{book_id}", headers: headers }

  let(:headers) do
    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  let(:access_token) { access_token_for(user) }

  let(:user) { create(:admin) }

  let(:book) { create(:book) }
  let(:book_id) { book.id }

  it_behaves_like "an endpoint under authorization"
  it_behaves_like "an endpoint under admin authorization"

  context "with valid admin jwt in headers" do
    it "succeeds" do
      subject

      expect(response).to have_http_status(:no_content)
    end

    it "deletes" do
      subject

      expect { book.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
