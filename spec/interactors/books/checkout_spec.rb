require "rails_helper"

RSpec.describe Books::Checkout do
  subject { described_class.call(user: user, book: book) }

  let(:user) { create(:user) }
  let(:book) { create(:book, available: available) }

  let(:checkout_scope) do
    BookCheckout.where(user_id: user.id, book_id: book.id)
  end

  context "when book is available" do
    let(:available) { true }

    it { is_expected.to be_success }

    it "creates a checkout for the user" do
      expect { subject }.to change(checkout_scope, :count).by(1)
    end

    it "set the book as not available" do
      expect { subject }.to change(book, :available).from(true).to(false)
    end
  end

  context "when book is not available" do
    let(:available) { false }

    it { is_expected.to be_failure }

    it "doesn't create a checkout" do
      expect { subject }.to_not change(checkout_scope, :count)
    end
  end
end
