require "rails_helper"

RSpec.describe Books::Return do
  subject { described_class.call(user: user, book: book) }

  let(:book) { create(:book, available: available) }
  let(:checkout_user) { create(:user) }
  let(:user) { checkout_user }

  let!(:checkout) { create(:book_checkout, book: book, user: checkout_user) }

  context "and book is available" do
    let(:available) { true }

    it { is_expected.to be_failure }
  end

  context "when book is not available" do
    let(:available) { false }

    context "and last checkout was performed by user" do
      it { is_expected.to be_success }

      it "set book as available" do
        expect { subject }.to change(book, :available).from(false).to(true)
      end
    end

    context "and last checkout was performed by another user" do
      let(:user) { create(:user) }

      it { is_expected.to be_failure }

      it "doens't set the book as available" do
        expect { subject }.to_not change(book, :available)
      end
    end
  end
end
