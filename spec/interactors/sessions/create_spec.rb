require "rails_helper"

RSpec.describe Sessions::Create do
  subject { described_class.call(email: email, password: password) }

  let(:email) { user.email }
  let(:password) { user.password }

  let(:user) { create(:user, email: "test@example.com", password: "Pass1234") }

  before { user }

  describe ".call" do
    it { is_expected.to be_success }

    context "with not existing email" do
      let(:email) { "not-existing@example.com" }

      it { is_expected.to be_failure }
    end

    context "with wrong password" do
      let(:password) { "another-password" }

      it { is_expected.to be_failure }
    end
  end
end
