module AuthenticationHelpers
  def headers_with_bearer(user)
    access_token = access_token_for(user)

    {
      "HTTP_AUTHORIZATION" => "Bearer #{access_token}"
    }
  end

  def access_token_for(user)
    Session.new(user: user).access_token
  end
end

RSpec.configure do |config|
  config.include AuthenticationHelpers, type: :request
end
