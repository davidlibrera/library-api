shared_examples_for "an endpoint under admin authorization" do
  context "without valid admin jwt in headers" do
    let(:headers) { headers_with_bearer(simple_user) }

    let(:simple_user) { create(:user, role: "user") }

    let(:expected_response) do
      {
        data: {
          id: be_a(String),
          type: "api_error",
          attributes: {
            code: "UNAUTHORIZED",
            details: {}
          }
        }
      }
    end

    it "returns unauthorized status" do
      subject

      expect(response).to have_http_status(:unauthorized)
    end

    it "returns an UNAUTHORIZED error" do
      subject

      expect(json_response).to match(expected_response)
    end
  end
end
