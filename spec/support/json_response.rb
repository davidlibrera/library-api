module JsonResponseHelpers
  def json_response
    JSON.parse(response.body).deep_symbolize_keys
  end

  def json_data
    json_response[:data]
  end
end

RSpec.configure do |config|
  config.include JsonResponseHelpers, type: :request
end
