class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :title, null: false
      t.string :isbn, null: false, index: {unique: true}

      t.timestamps
    end
  end
end
