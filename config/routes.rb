Rails.application.routes.draw do
  namespace :api, defaults: {format: :json} do
    resources :books, only: [:index, :show, :create, :update, :destroy] do
      scope module: :books do
        resources :checkouts, only: [:index]
      end
    end

    resources :users, only: [:index, :show, :create, :update, :destroy]

    resource :session, only: [:create]
  end
end
