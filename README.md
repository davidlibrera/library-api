# Library-Api

## Setup & Requirements

* ruby `2.7.6`
* postgresql `12`

The project has a configuration both for versions with [asdf](https://asdf-vm.com/)
and for application services with [docker-compose](https://docs.docker.com/compose/)

If you want to use these tools, simply install it following the instructions:

* [Asdf instuctions](https://asdf-vm.com/guide/getting-started.html#_1-install-dependencies)
* [Asdf ruby plugin](https://github.com/asdf-vm/asdf-ruby)
* [Docker-compose instructions](https://docs.docker.com/compose/install/)

When tools are configured, you can install all versions typing `asdf install`.

## Development
To start the development environment, start docker-compose typing `docker-compose up`.

After the system is ready, configure and initialize the database running `rails db:setup`

## Linter
The code style is managed using [rubocop](https://github.com/rubocop/rubocop)

To run the linter, type `bundle exec rubocop`.

## Testing
To run tests, type `bundle exec rspec`

## Run the suite pipeline
Typing `bundle exec rake` you'll run both linter and tests. It is highly
recommended that you run this command before a deploy.
